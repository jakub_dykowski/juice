<?php
namespace migit\inject;

use migit\inject\binder\LinkedBindingBuilder;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
interface Binder {

// 	function bindInterceptor(Matcher $classMatcher, Matcher $methodMatcher
// 	      /*guess not used MethodInterceptor $interceptors*/);

	/**
	 * See the EDSL examples at {@link Binder}.
	 *
	 * @param
	 *        	Key whatever else
	 * @return LinkedBindingBuilder
	 */
	function bind($key);
	
	// TODO important for binding to class
	/**
	 * See the EDSL examples at {@link Binder}.
	 */
	// <T> AnnotatedBindingBuilder<T> bind(Class<T> type);
	
	// /**
	// * Upon successful creation, the {@link Injector} will inject instance fields
	// * and methods of the given object.
	// *
	// * @param TypeLiteral $type
	// * of instance
	// * @param object $instance
	// * for which members will be injected
	// * @since 2.0
	// */
	// function requestInjection(TypeLiteral $type, $instance);
	
	/**
	 * Upon successful creation, the {@link Injector} will inject instance fields
	 * and methods of the given object.
	 *
	 * @param object $instance
	 *        	for which members will be injected
	 * @since 2.0
	 */
	function requestInjection($instance);

// 	/**
// 	 * Upon successful creation, the {@link Injector} will inject static fields
// 	 * and methods in the given classes.
// 	 *
// 	 * @param $types,... for
// 	 *        	which static members will be injected
// 	 */
// 	function requestStaticInjection($types);

	/**
	 * Uses the given module to configure more bindings.
	 */
	function install(Module $module);

// 	/**
// 	 * Gets the current stage.
// 	 *
// 	 * @return int Stage
// 	 */
// 	function currentStage();

// 	/**
// 	 * Records an error message which will be presented to the user at a later
// 	 * time.
// 	 * Unlike throwing an exception, this enable us to continue
// 	 * configuring the Injector and discover more errors. Uses {@link
// 	 * String#format(String, Object[])} to insert the arguments into the
// 	 * message.
// 	 *
// 	 * @param string $message        	
// 	 * @param mixed $arguments,...        	
// 	 */
// 	function addError($message, $arguments);

// 	/**
// 	 * Records an exception, the full details of which will be logged, and the
// 	 * message of which will be presented to the user at a later
// 	 * time.
// 	 * If your Module calls something that you worry may fail, you should
// 	 * catch the exception and pass it into this.
// 	 */
// 	function addError(\Exception $e);

// 	/**
// 	 * Records an error message to be presented to the user at a later time.
// 	 *
// 	 * @since 2.0
// 	 */
// 	function addError(Message $message);

// 	/**
// 	 * Returns the provider used to obtain instances for the given injection key.
// 	 * The returned provider will not be valid until the {@link Injector} has been
// 	 * created. The provider will throw an {@code IllegalStateException} if you
// 	 * try to use it beforehand.
// 	 *
// 	 * @return Provider
// 	 * @since 2.0
// 	 */
// 	function getProvider(Key $key);

	/**
	 * Returns the provider used to obtain instances for the given injection type.
	 * The returned provider will not be valid until the {@link Injector} has been
	 * created. The provider will throw an {@code IllegalStateException} if you
	 * try to use it beforehand.
	 *
	 * @param mixed $type
	 * @return Provider
	 * @since 2.0
	 */
	function getProvider($type);

// 	/**
// 	 * Returns the members injector used to inject dependencies into methods and fields on instances
// 	 * of the given type {@code T}.
// 	 * The returned members injector will not be valid until the main
// 	 * {@link Injector} has been created. The members injector will throw an {@code
// 	 * IllegalStateException} if you try to use it beforehand.
// 	 *
// 	 * @param
// 	 *        	typeLiteral type to get members injector for
// 	 * @return MembersInjector
// 	 * @since 2.0
// 	 */
// 	function getMembersInjector(TypeLiteral $typeLiteral);

// 	/**
// 	 * Returns the members injector used to inject dependencies into methods and fields on instances
// 	 * of the given type {@code T}.
// 	 * The returned members injector will not be valid until the main
// 	 * {@link Injector} has been created. The members injector will throw an {@code
// 	 * IllegalStateException} if you try to use it beforehand.
// 	 *
// 	 * @param class $type
// 	 *        	to get members injector for
// 	 * @return MembersInjector
// 	 * @since 2.0
// 	 */
// 	function getMembersInjector($type);

// 	/**
// 	 * Binds a type converter.
// 	 * The injector will use the given converter to
// 	 * convert string constants to matching types as needed.
// 	 *
// 	 * @param
// 	 *        	typeMatcher matches types the converter can handle
// 	 * @param
// 	 *        	converter converts values
// 	 * @since 2.0
// 	 */
// 	function convertToTypes(Matcher $typeMatcher, TypeConverter $converter);

// 	/**
// 	 * Registers a listener for injectable types.
// 	 * Guice will notify the listener when it encounters
// 	 * injectable types matched by the given type matcher.
// 	 *
// 	 * @param
// 	 *        	typeMatcher that matches injectable types the listener should be notified of
// 	 * @param
// 	 *        	listener for injectable types matched by typeMatcher
// 	 * @since 2.0
// 	 */
// 	function bindListener(Matcher $typeMatcher, TypeListener $listener);

// 	/**
// 	 * Registers listeners for provisioned objects.
// 	 * Guice will notify the
// 	 * listeners just before and after the object is provisioned. Provisioned
// 	 * objects that are also injectable (everything except objects provided
// 	 * through Providers) can also be notified through TypeListeners registered in
// 	 * {@link #bindListener}.
// 	 *
// 	 * @param
// 	 *        	bindingMatcher that matches bindings of provisioned objects the listener
// 	 *        	should be notified of
// 	 * @param ProvisionListener $listeners,...
// 	 *        	for provisioned objects matched by bindingMatcher
// 	 * @since 4.0
// 	 */
// 	function bindListener(Matcher $bindingMatcher, $listeners);

// 	/**
// 	 * Returns a binder that uses {@code source} as the reference location for
// 	 * configuration errors.
// 	 * This is typically a {@link StackTraceElement}
// 	 * for {@code .java} source but it could any binding source, such as the
// 	 * path to a {@code .properties} file.
// 	 *
// 	 * @param object $source
// 	 *        	any object representing the source location and has a
// 	 *        	concise {@link Object#toString() toString()} value
// 	 * @return Binder a binder that shares its configuration with this binder
// 	 * @since 2.0
// 	 */
// 	function withSource($source);

// 	/**
// 	 * Returns a binder that skips {@code classesToSkip} when identify the
// 	 * calling code.
// 	 * The caller's {@link StackTraceElement} is used to locate
// 	 * the source of configuration errors.
// 	 *
// 	 * @param class $classesToSkip,...
// 	 *        	library classes that create bindings on behalf of
// 	 *        	their clients.
// 	 * @return Binder a binder that shares its configuration with this binder.
// 	 * @since 2.0
// 	 */
// 	function skipSources($classesToSkip);

// 	/**
// 	 * Creates a new private child environment for bindings and other configuration.
// 	 * The returned
// 	 * binder can be used to add and configuration information in this environment. See {@link
// 	 * PrivateModule} for details.
// 	 *
// 	 * @return PrivateBinder a binder that inherits configuration from this binder. Only exposed configuration on
// 	 *         the returned binder will be visible to this binder.
// 	 * @since 2.0
// 	 */
// 	function newPrivateBinder();

// 	/**
// 	 * Instructs the Injector that bindings must be listed in a Module in order to
// 	 * be injected.
// 	 * Classes that are not explicitly bound in a module cannot be
// 	 * injected. Bindings created through a linked binding (
// 	 *
// 	 * <code>bind(Foo.class).to(FooImpl.class)</code>
// 	 * ) are allowed, but the * implicit binding (
// 	 * <code>FooImpl</code>
// 	 * ) cannot be directly injected unless * it is also explicitly bound (
// 	 * <code>bind(FooImpl.class)</code>
// 	 * ). *
// 	 * <p>* Tools can still retrieve bindings for implicit bindings (bindings
// 	 * created * through a linked binding) if explicit bindings are required,
// 	 * however * {@link Binding#getProvider} will fail. *
// 	 * <p>* By default, explicit bindings are not required. *
// 	 * <p>* If a parent injector requires explicit bindings, then all child
// 	 * injectors * (and private modules within that injector) also require
// 	 * explicit bindings. * If a parent does not require explicit bindings, a
// 	 * child injector or private * module may optionally declare itself as
// 	 * requiring explicit bindings. If it * does, the behavior is limited only
// 	 * to that child or any grandchildren. No * siblings of the child will
// 	 * require explicit bindings. *
// 	 * <p>* If the parent did not require explicit bindings but the child does,
// 	 * it is * possible that a linked binding in the child may add a JIT
// 	 * binding to the * parent. The child will not be allowed to reference the
// 	 * target binding * directly, but the parent and other children of the
// 	 * parent may be able to. * * @since 3.0
// 	 */
// 	function requireExplicitBindings();

// 	/**
// 	 * * Prevents Guice from constructing a
// 	 * {@link Proxy} when a circular dependency * is found.
// 	 * By default,
// 	 * circular proxies are not disabled. *
// 	 * <p>* If a parent injector disables circular proxies, then all child
// 	 * injectors * (and private modules within that injector) also disable
// 	 * circular proxies. * If a parent does not disable circular proxies, a
// 	 * child injector or private * module may optionally declare itself as
// 	 * disabling circular proxies. If it * does, the behavior is limited only
// 	 * to that child or any grandchildren. No * siblings of the child will
// 	 * disable circular proxies. * * @since 3.0
// 	 */
// 	function disableCircularProxies();

// 	/**
// 	 * * Requires that a {@literal @}{@link
// 	 * Inject} annotation exists on a constructor in order for * Guice to
// 	 * consider it an eligible injectable class.
// 	 * By default, Guice will inject
// 	 * classes that * have a no-args constructor if no {@literal @}{@link
// 	 * Inject} annotation exists on any * constructor. *
// 	 * <p>
// 	 * If the class is bound using {@link
// 	 * LinkedBindingBuilder#toConstructor}, Guice will still inject * that
// 	 * constructor regardless of annotations. * * @since 4.0
// 	 */
// 	function requireAtInjectOnConstructors();

// 	/**
// 	 * * Requires that Guice finds an
// 	 * exactly matching binding annotation.
// 	 * This disables the * error-prone
// 	 * feature in Guice where it can substitute a binding for *
// 	 * <code>{@literal @}Named Foo</code>
// 	 * when attempting to inject *
// 	 * <code>{@literal @}Named("foo") Foo</code>
// 	 * . * * @since 4.0
// 	 */
// 	function requireExactBindingAnnotations();
}