<?php
namespace migit\inject;

use InvalidArgumentException;
use migit\inject\impl\BindingInstantiator;
use migit\inject\impl\DefaultBinder;
use migit\inject\impl\DefaultScope;
use migit\inject\impl\InjectorImpl;
use ReflectionClass;
use RuntimeException;

/**
 * The entry point to the Juice framework. Creates {@link Injector}s from {@link Module}s.
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 * @license MIT
 */
final class Juice {

	private function __construct() {}

    /**
     *
     * @param int $stage default Stage::DEVELOPMENT
     * @param Module[] $modules
     * @return Injector
     */
	public static function createInstance($stage = null, Module ...$modules) {
		if ($stage === null) {
			$stage = Stage::DEVELOPMENT;
		} elseif (!in_array($stage, [Stage::values()])) {
			throw new InvalidArgumentException("invalid stage");
		}

		switch ($stage) {
			case Stage::DEVELOPMENT:
				break;
			case Stage::PRODUCTION:
				break;
			case Stage::TOOL:
				throw new RuntimeException("Stage::TOOL not implemented yet, probably ever");
			default:
				throw new InvalidArgumentException("unknown stage");
		}

		
//		$injectorReflection = $reflectionService->getClass(DefaultInjector::class);
//		$injector = $injectorReflection->newInstanceWithoutConstructor();
//		$injector = new DefaultInjector();

		// first we create injector without calling constructor, due to weird circullar dependencies
		$injectorReflection = new ReflectionClass(InjectorImpl::class);
        /** @var Injector $injector */
		$injector = $injectorReflection->newInstanceWithoutConstructor();
//		$injector = new InjectorImpl();

		// $genericProvider = new GenericProvider($injector); not necessary anymore
//		$dependenciesProvider = new DefaultDependenciesProvider($reflectionService, $injector);
		// $providesProvider = new ProvidesProvider($dependenciesProvider);
		$binder = new DefaultBinder($injector/*, $providesProvider*/);
		$defaultScope = new DefaultScope($injector);
		$bindingInstantiator = new BindingInstantiator($defaultScope, $injector);
		// $injectorClass->invokeConstructorIfExists($injector, array (
		// $binder,
		// $bindingInstantiator,
		// $reflectionService,
		// $dependenciesProvider
		// ));
		
		// finally we call injector constructor
		$injectorReflection->getConstructor()->invokeArgs($injector, array (
				null,
				$binder,
				$bindingInstantiator,
// 				$reflectionService,
// 				$dependenciesProvider 
		));
		
// 		$binder->bind('\inject\impl\DependenciesProvider')->to('\inject\impl\DefaultDependenciesProvider')->inSingleton();
// 		$binder->bind('\inject\Binder')->toInstance($binder);
// 		$binder->bind('\inject\binding\Bindings')->toInstance($binder);
// 		$binder->bind('\inject\reflect\ReflectionService')->toInstance($reflectionService);
// 		$binder->bind('\inject\Injector')->toInstance($injector);
// 		$binder->bind('\inject\impl\MembersInjector')->toInstance($injector);
// 		$binder->bind('\inject\impl\Singleton')->toInstance(new Singleton($defaultScope));

		$i = 1;
		foreach ($modules as $module) {
			if ($module instanceof Module) {
                if (DEBUG) echo "installing module " . get_class($module) . PHP_EOL;
				$binder->install($module);
			} else {
				throw new InvalidArgumentException("Received non-module argument at position: $i");
			}
			++$i;
		}

		// also bind itself
        $binder->bind(Injector::class)->toInstance($injector);
		
		$binder->build();
		// $bindingValidator = new BindingValidator();
		// $bindingValidator->validateAll($binder->getBindings());

		return $injector;
	}
}