<?php
namespace migit\inject;

use migit\inject\binding\BindingVisitor;
use RuntimeException;

interface Binding {

	/**
	 * Returns the key for this binding.
	 * @return mixed
	 */
	function getKey();

	/**
	 * Returns the scoped provider guice uses to fulfill requests for this
	 * binding.
	 *
	 * @return Provider
	 * @throws RuntimeException when invoked on a {@link Binding}
	 *         created via {@link com.google.inject.spi.Elements#getElements}. This
	 *         method is only supported on {@link Binding}s returned from an injector.
	 */
	function getProvider();

	/**
	 * Accepts a target visitor.
	 * Invokes the visitor method specific to this binding's target.
	 * 
	 * @param BindingVisitor $bindingVisitor        	
	 */
	function accept(BindingVisitor $bindingVisitor);
	
	// /**
	// * Accepts a target visitor. Invokes the visitor method specific to this binding's target.
	// *
	// * @param visitor to call back on
	// * @since 2.0
	// */
	// function /*<V> V */acceptTargetVisitor(BindingTargetVisitor $visitor);
	
	// /**
	// * Accepts a scoping visitor. Invokes the visitor method specific to this binding's scoping.
	// *
	// * @param visitor to call back on
	// * @since 2.0
	// */
	// <V> V acceptScopingVisitor(BindingScopingVisitor<V> visitor);
}