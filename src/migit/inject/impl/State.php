<?php
namespace migit\inject\impl;

use migit\inject\Key;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
interface State {

	/** State|null */
	function parent();

	/**
	 * Gets a binding which was specified explicitly in a module, or null.
	 * @param Key $key
	 * @return BindingImpl
	 */
	function getExplicitBinding(Key $key);

	/**
	 * Returns the explicit bindings at this level only.
	 * @todo create some hashmap with objects as keys
	 * @return map Key -> Binding
	 */
	function getExplicitBindingsThisLevel();

	function putBinding(Key $key, BindingImpl $binding);

	function addConverter(TypeConverterBinding $typeConverterBinding);

/**
 * Returns the matching converter for {@code type}, or null if none match.
 * @param string $stringValue
 * @param TypeLiteral $type
 * @param Errors $errors
 * @return TypeConverterBinding
 */
	function getConverter($stringValue, TypeLiteral $type, Errors $errors, $source);

	/**
	 * Returns all converters at this level only.
	 * @return TypeConverterBinding[]
	 */
	function getConvertersThisLevel();

	/**
	 * Forbids the corresponding injector from creating a binding to {@code key}. Child injectors
	 * blacklist their bound keys on their parent injectors to prevent just-in-time bindings on the
	 * parent injector that would conflict.
	 * @param Key $key
	 * @param object $source
	 * @return
	 */
	function blacklist(Key $key, $source);

	/**
	 * Returns true if {@code key} is forbidden from being bound in this injector. This indicates that
	 * one of this injector's descendent's has bound the key.
	 * @param Key $key
	 * @return bool
	 */
	function isBlacklisted(Key $key);

	/**
	 * Returns the source of a blacklisted key.
	 * @param Key $key
	 * @return object[]
	 */
	function getSourcesForBlacklistedKey(Key $key);
}