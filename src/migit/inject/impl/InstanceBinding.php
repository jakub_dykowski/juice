<?php
namespace migit\inject\impl;

use migit\inject\binding\BindingVisitor;
use migit\inject\Provider;
use migit\inject\reflect\Clazz;
use ReflectionClass;
use RuntimeException;

class InstanceBinding extends AbstractScopedBinding {

	private $instance;

	public function __construct(ReflectionClass $source, $instance, ReflectionClass $scope = null) {
		$this->source = $source;
		$this->instance = $instance;
		$this->scope = $scope;
	}

	public function getInstance() {
		return $this->instance;
	}

	public function accept(BindingVisitor $bindingVisitor) {
		return $bindingVisitor->visitInstanceBinding($this);
	}

    function getProvider() {
        return new InstanceProvider($this->instance);
    }
}
