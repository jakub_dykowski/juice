<?php
namespace migit\inject\impl;

interface MemberInjector {

	function injectTo($instance);
}
