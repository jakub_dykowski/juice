<?php
namespace migit\inject\impl;

use migit\inject\Key;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class InheritingState implements State {

	/** @var  State */
	private $parent;

	function parent() {
		return $this->parent;
	}

	function getExplicitBinding(Key $key) {
		// TODO: Implement getExplicitBinding() method.
	}

	function getExplicitBindingsThisLevel() {
		// TODO: Implement getExplicitBindingsThisLevel() method.
	}

	function putBinding(Key $key, BindingImpl $binding) {
		// TODO: Implement putBinding() method.
	}

	function addConverter(TypeConverterBinding $typeConverterBinding) {
		// TODO: Implement addConverter() method.
	}

	function getConverter($stringValue, TypeLiteral $type, Errors $errors, $source) {
		// TODO: Implement getConverter() method.
	}

	function getConvertersThisLevel() {
		// TODO: Implement getConvertersThisLevel() method.
	}

	function blacklist(Key $key, $source) {
		// TODO: Implement blacklist() method.
	}

	function isBlacklisted(Key $key) {
		// TODO: Implement isBlacklisted() method.
	}

	function getSourcesForBlacklistedKey(Key $key) {
		// TODO: Implement getSourcesForBlacklistedKey() method.
	}
}