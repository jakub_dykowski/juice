<?php
namespace migit\inject\impl;

use migit\inject\binding\Bindings;
use migit\inject\Injector;
use migit\inject\Provider;
use ReflectionClass;
use RuntimeException;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 *
 * @deprecated
 */
class DefaultInjector implements Injector, MembersInjector {

	/**
	 * @var Bindings
	 */
	private $bindings;
	private $bindingInstantiator;
	private $dependenciesProvider;
	private $dependencies = array ();
	private $incompleteInstances = array ();

	public function __construct(Bindings $bindings = null, BindingInstantiator $bindingInstantiator = null, DependenciesProvider $dependenciesProvider = null) {
		$this->bindings = $bindings;
		$this->bindingInstantiator = $bindingInstantiator;
		$this->dependenciesProvider = $dependenciesProvider;
	}

	public function getInstance($qualifiedClassName) {
		$binding = $this->bindings->getOrCreateBinding($qualifiedClassName);
		return $this->bindingInstantiator->getInstance($binding);
	}

	/**
	 * @deprecated don't know why yet
	 * @param $qualifiedClassName
	 * @return Provider
	 */
	public function getProviderFor($qualifiedClassName) {
		throw new RuntimeException("obsolete");
//		return $this->dependenciesProvider->getProviderFor($qualifiedClassName);
	}

	function getProvider($qualifiedClassName) {
		// TODO implement :inject
		return $this->dependenciesProvider->getProviderFor($qualifiedClassName);
	}

	public function getConstant($constant) {
		$constantBinding = $this->bindings->getConstantBinding($constant);

		if ($constantBinding == null) {
			throw new RuntimeException('No binding found for constant: ' . $constant);
		}

		return $constantBinding->getValue();
	}

	public function createInstance($qualifiedClassName) {
		$class = new ReflectionClass($qualifiedClassName);
		$this->incompleteInstances += array (
				$qualifiedClassName => array ()
		);

		if (isset($this->dependencies[$qualifiedClassName])) {
			$incompleteInstance = $class->newInstanceWithoutConstructor();
			$this->incompleteInstances[$qualifiedClassName][] = $incompleteInstance;
			return $incompleteInstance;
		}

		$this->dependencies[$qualifiedClassName] = true;
		$constructorInjector = $this->getConstructorInjector($class);
		$memberInjectors = $this->getMemberInjectors($class);
		$instance = $constructorInjector->createNew();

		foreach ($memberInjectors as $injector) {
			$injector->injectTo($instance);
		}

		foreach ($this->incompleteInstances[$qualifiedClassName] as $incompleteInstance) {
			$constructorInjector->injectTo($incompleteInstance);

			foreach ($memberInjectors as $injector) {
				$injector->injectTo($incompleteInstance);
			}
		}

		unset($this->dependencies[$qualifiedClassName]);
		unset($this->incompleteInstances[$qualifiedClassName]);
		return $instance;
	}

	public function injectTo($instance) {
		$class = new ReflectionClass(get_class($instance));

		foreach ($this->getMemberInjectors($class) as $membersInjector) {
			$membersInjector->injectTo($instance);
		}
	}

	function injectMembers($instance) {
		// TODO implement :inject
		throw new RuntimeException("not implemented yet");
	}

	function getParent() {
		// TODO implement :inject
		throw new RuntimeException("not implemented yet");
	}

	function createChildInjector($module) {
		// TODO implement :inject
		throw new RuntimeException("not implemented yet");
	}

	/**
	 *
	 * @param ReflectionClass $class
	 * @return ConstructorInjector
	 */
	private function getConstructorInjector(ReflectionClass $class) {
		$dependencies = $this->dependenciesProvider->getConstructorDependencies($class->getName());
		return new ConstructorInjector($class, $dependencies);
	}

	/**
	 *
	 * @param ReflectionClass $class
	 * @return MemberInjector[]
	 */
	private function getMemberInjectors(ReflectionClass $class) {
		$injectors = array ();
		// TODO :annotations
		// $injectableFields = $class->getFieldsWithAnnotation(Annotations::INJECT);

		// foreach ($injectableFields as $field) {
		// if (!$field->isStatic()) {
		// $dependency = $this->dependenciesProvider->getDependencyOfField($field);
		// $injectors[] = new FieldInjector($field, $dependency);
		// }
		// }

		// TODO :annotations
		// $injectableMethods = $class->getMethodsWithAnnotation(Annotations::INJECT);
		// foreach ($injectableMethods as $method) {
		// if (!$method->isStatic()) {
		// $dependencies = $this->dependenciesProvider->getDependenciesOfMethod($method);
		// $injectors[] = new MethodInjector($method, $dependencies);
		// }
		// }

		return $injectors;
	}
}