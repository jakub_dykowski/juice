<?php
namespace migit\inject\impl;

use inject\impl\BindingScopingVisitor;
use inject\impl\ElementVisitor;
use inject\impl\InternalFactory;
use inject\impl\spi\InstanceBinding;
use LogicException;
use migit\inject\Binding;
use migit\inject\Key;
use migit\inject\Provider;

class BindingImpl implements Binding {
	/**
	 *
	 * @var InjectorImpl
	 */
	private $injector;
	/**
	 *
	 * @var Key
	 */
	private $key;
	/**
	 *
	 * @var object
	 */
	private $source;
	/**
	 *
	 * @var Scoping
	 */
	private $scoping;
	/**
	 *
	 * @var InternalFactory
	 */
	private $internalFactory;
	/**
	 *
	 * @var Provider
	 */
	private $provider;

	function __construct(InjectorImpl $injector = null, Key $key, $source/*, InternalFactory $internalFactory = null*/, Scoping $scoping) {
		$this->injector = $injector;
		$this->key = $key;
		$this->source = $source;
		$this->internalFactory = $internalFactory;
		$this->scoping = $scoping;
	}

	function getKey() {
		return $this->key;
	}

	function getSource() {
		return $this->source;
	}

	function getProvider() {
		if ($this->provider == null) {
			if ($this->injector == null) {
				throw new LogicException("getProvider() not supported for module bindings");
			}
			
			$this->provider = $injector->getProvider($key);
		}
		return $this->provider;
	}

	function getInternalFactory() {
		// TODO implement :inject
		return $this->internalFactory;
	}

	function getScoping() {
		return $scoping;
	}

	/**
	 * Is this a constant binding? This returns true for constant bindings as
	 * well as toInstance() bindings.
	 * 
	 * @return bool
	 */
	function isConstant() {
		return $this instanceof InstanceBinding;
	}
	// TODO copy ElementVisitor
	function acceptVisitor(ElementVisitor $visitor) {
		return $visitor->visit($his);
	}

	function acceptScopingVisitor(BindingScopingVisitor $visitor) {
		return $this->scoping->acceptVisitor($visitor);
	}

	protected function withScoping(Scoping $scoping) {
		throw new LogicException('assertion error');
	}

	protected function withKey(Key $key) {
		throw new LogicException('assertion error');
	}

	function withRehashedKeys() {
		throw new LogicException('assertion error');
	}

	public function getInjector() {
		return $this->injector;
	}
}