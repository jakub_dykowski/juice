<?php
namespace migit\inject\impl;

use Exception;
use migit\inject\Binder;
use migit\inject\binder\LinkedBindingBuilder;
use migit\inject\Binding;
use migit\inject\binding\Bindings;
use migit\inject\ConfigurationException;
use migit\inject\Injector;
use migit\inject\Module;
use ReflectionClass;
use RuntimeException as SourceAlreadyBoundException;

class DefaultBinder implements Binder, Bindings {

	private $linkedBindingBuilder;

	/**
	 * @var Injector
	 */
	private $injector;

	// FIXME remove unused field
	private $providesProvider;
	
	/**
	 *
	 * @var Module[]
	 */
	private $modules = [];
	
	/**
	 *
	 * @var Binding[]
	 */
	private $bindings = [];

	public function __construct(Injector $injector/*, ProvidesProvider $providesProvider*/) {
        // FIXME rename membersInjector or trow him out
		$this->linkedBindingBuilder = new LinkedBindingBuilder($this, $injector);// FIXME $membersInjector is really Injector, should not be here
		$this->injector = $injector;
		// $this->providesProvider = $providesProvider;
	}

	/**
	 *
	 * @param string $qualifiedClassName        	
	 * @return LinkedBindingBuilder
	 */
	public function bind($qualifiedClassName) {
		return $this->linkedBindingBuilder->bind($qualifiedClassName);
	}

	/**
	 *
	 * @param string $constant        	
	 * @return ConstantBinding
	 */
	public function bindConstant($constant) {
		$binding = new ConstantBinding($constant);
		$this->addBinding($binding);
		return $binding;
	}

	public function install(Module $module) {
		$module->configureBinder($this); // TODO :change jacob
		$this->modules[] = $module;
	}

	public function build() {
		// TODO building binder should take more than just finding annotations. The mock bindings should be replaced with real ones
        // annotations are not supported, since PHP does not support them YET
//        foreach ($this->modules as $module) {
//			break;
//		    $moduleClass = $this->reflectionService->getClass(get_class($module));
//			$providesMethods = $moduleClass->getMethodsWithAnnotation(Annotations::PROVIDES);
//
//			foreach ($providesMethods as $method) {
//				$this->bindAsProvider($method, $module);
//			}
//		}
		
		return $this;
	}

	public function addBinding(Binding $binding) {
		$key = $binding->getKey();
		
		if (isset($this->bindings[$key])) {
			throw new SourceAlreadyBoundException($key . ' is already bound');
		}
		
		$this->bindings[$key] = $binding;
	}

	/**
	 *
	 * @param string $key        	
	 * @return Binding
	 */
	public function getBinding($key) {
		$key = ltrim($key, '\\');
		return isset($this->bindings[$key]) ? $this->bindings[$key] : null;
	}

	/**
	 *
	 * @param string $constant        	
	 * @return Binding
	 */
	public function getConstantBinding($constant) {
		return $this->getBinding(ConstantBinding::generateKey($constant));
	}

	/**
	 *
	 * @param string $key
	 * @return Binding
	 * @throws ConfigurationException
	 */
	public function getOrCreateBinding($key) {
		$key = ltrim($key, '\\');
		
		if (!isset($this->bindings[$key])) {
		    // FIXME not so sure if its good to assume that key is the class

            // we cannot create JIT binding for an interface, only for a class
		    if (interface_exists($key))
                throw new ConfigurationException("binding for interface not found: $key");

			// FIXME, thats not jit binding, that is
			// Just-in-time binding
			// probably old implementation
//			$this->bind($key);

			// For future reference:
			// If we want JIT bindings to return singleton instance, we should bind in singleton scope.
			// If we want JIT bindings to return new instance each time we should bind in no scope.

			// register JIT binding so this and the next call will return the same binding
			$class = new ReflectionClass($key);
			// until we have working scopes, use SingletonProvider for the binding to produce singleton
			$jitProvider = new SingletonProvider(new JustInTimeProvider($this->injector, $class));
			$this->addBinding(new ProviderInstanceBinding($class, $jitProvider));
		}

		return $this->getBinding($key);
	}

	/**
	 *
	 * @return Binding[]
	 */
	public function getBindings() {
		return $this->bindings;
	}

	public function removeBinding($key) {
		unset($this->bindings[$key]);
	}
	
	function getProvider($type) {
		// it looks like this binder is used when configuring modules
		// TODO :inject implement, before that decide if inside module configuration module should be available

		// so temporarily:
		return $this->injector->getProvider($type);

//		var_dump("here!!!!!!!");
//		var_dump($type);
//		var_dump($this->bindings['type']);
//		flush();
//		return $this->bindings[$type]->getProvider();
//		throw new RuntimeException("not implemented yet");
	}

	public function requestInjection($instance) {
		// TODO: Implement requestInjection() method.
		throw new Exception("not supported (old crap removed)");

		// that is old crap
		$this->injector->injectTo($instance);
	}
}
