<?php
namespace migit\inject\impl;

use migit\inject\Binding;
use migit\inject\Provider;

/**
 * Temporary workaround.
 * @author kuba
 * @deprecated
 */
class InjectingProvider implements Provider {

	/** @var BindingInstantiator  */
	private $instantiator;

	/** @var Binding  */
	private $binding;

	/**
	 * @deprecated see class description
	 */
	function __construct(BindingInstantiator $instantiator, Binding $binding) {
		$this->instantiator = $instantiator;
		$this->binding = $binding;
	}

	function get() {
		return $this->instantiator->getInstance($this->binding);
	}
}