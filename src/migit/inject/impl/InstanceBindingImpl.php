<?php
namespace migit\inject\impl;

use inject\InstanceBinding;
use migit\inject\binding\BindingVisitor;

final class InstanceBindingImpl extends BindingImpl implements InstanceBinding {
/*
	final T instance;
	final Provider<T> provider;
	final ImmutableSet<InjectionPoint> injectionPoints;

	public InstanceBindingImpl(InjectorImpl injector, Key<T> key, Object source,
			InternalFactory<? extends T> internalFactory, Set<InjectionPoint> injectionPoints,
			T instance) {
		super(injector, key, source, internalFactory, Scoping.EAGER_SINGLETON);
		this.injectionPoints = ImmutableSet.copyOf(injectionPoints);
		this.instance = instance;
		this.provider = Providers.of(instance);
	}

	public InstanceBindingImpl(Object source, Key<T> key, Scoping scoping,
			Set<InjectionPoint> injectionPoints, T instance) {
		super(source, key, scoping);
		this.injectionPoints = ImmutableSet.copyOf(injectionPoints);
		this.instance = instance;
		this.provider = Providers.of(instance);
	}

	@Override public Provider<T> getProvider() {
		return this.provider;
	}

	public <V> V acceptTargetVisitor(BindingTargetVisitor<? super T, V> visitor) {
		return visitor.visit(this);
	}

	public T getInstance() {
		return instance;
	}

	public Set<InjectionPoint> getInjectionPoints() {
		return injectionPoints;
	}

	public Set<Dependency<?>> getDependencies() {
    return instance instanceof HasDependencies
        ? ImmutableSet.copyOf(((HasDependencies) instance).getDependencies())
        : Dependency.forInjectionPoints(injectionPoints);
  }

  public BindingImpl<T> withScoping(Scoping scoping) {
    return new InstanceBindingImpl<T>(getSource(), getKey(), scoping, injectionPoints, instance);
  }

  public BindingImpl<T> withKey(Key<T> key) {
    return new InstanceBindingImpl<T>(getSource(), key, getScoping(), injectionPoints, instance);
  }

  public BindingImpl<T> withRehashedKeys() {
    if (needsRehashing(getKey())) {
      return withKey(rehash(getKey()));
    } else {
      return this;
    }
  }

  public void applyTo(Binder binder) {
    // instance bindings aren't scoped
    binder.withSource(getSource()).bind(getKey()).toInstance(instance);
  }

  @Override public String toString() {
    return Objects.toStringHelper(InstanceBinding.class)
        .add("key", getKey())
        .add("source", getSource())
        .add("instance", instance)
        .toString();
  }

  @Override
  public boolean equals(Object obj) {
    if(obj instanceof InstanceBindingImpl) {
      InstanceBindingImpl<?> o = (InstanceBindingImpl<?>)obj;
      return getKey().equals(o.getKey())
        && getScoping().equals(o.getScoping())
        && Objects.equal(instance, o.instance);
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getKey(), getScoping());
  }*/
	/**
	 * Accepts a target visitor.
	 * Invokes the visitor method specific to this binding's target.
	 *
	 * @param BindingVisitor $bindingVisitor
	 */
	function accept(BindingVisitor $bindingVisitor) {
		// TODO: Implement accept() method.
		throw new \RuntimeException("not implemented yet");
	}
}
