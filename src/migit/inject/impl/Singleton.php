<?php
namespace migit\inject\impl;

use migit\inject\Binding;

class Singleton implements ShittyScope {
	private $defaultScope;
	private $instances = array ();

	public function __construct(DefaultScope $defaultScope) {
		$this->defaultScope = $defaultScope;
	}

	public function getInstance(Binding $binding) {
		$key = $binding->getKey();
		
		if (!isset($this->instances[$key])) {
			$this->instances[$key] = $this->defaultScope->getInstance($binding);
		}
		
		return $this->instances[$key];
	}
}
