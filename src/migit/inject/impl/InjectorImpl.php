<?php
namespace migit\inject\impl;

use migit\inject\binding\Bindings;
use migit\inject\Injector;
use ReflectionClass;
use RuntimeException;

/**
 * Thats is probably the most current, and mostly implemented implementation of Injector.
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class InjectorImpl implements Injector, MembersInjector {

	/**
	 *
	 * @var InjectorImpl
	 */
	private $parent;

	/**
	 * Shared with parets and childs
	 * @todo jakiego typu to ma być?
	 * @var null null for now
	 */
	protected $state;

	/**
	 *
	 * @var Bindings
	 */
	protected $bindings;

	/**
	 *
	 * @var BindingInstantiator
	 */
	private $bindingInstantiator;

	private $dependenciesProvider;

	private $dependencies = array();
	private $incompleteInstances = array();

	function __construct(InjectorImpl $parent = null, Bindings $bindings, BindingInstantiator $bindingInstantiator,
						 $options = null) {
		// TODO :inject replace bindings with state
		$this->parent = $parent;
		$this->state = null;
		$this->options = $options;

		$this->bindings = $bindings;
		$this->bindingInstantiator = $bindingInstantiator;
		$this->dependenciesProvider = new DefaultDependenciesProvider($this);
	}

	function injectMembers($instance) {
		// TODO is this working :old
		$class = new ReflectionClass(get_class($instance));

		foreach ($this->getMemberInjectors($class) as $injector) {
			$injector->injectTo($instance);
		}
	}

	/**
	 * @deprecated remove this shit
	 */
	function injectTo($instance) {
		return $this->injectMembers($instance);
	}

	function getProvider($type) {
		if (DEBUG) echo "DEBUG " . __METHOD__ . "($type)" . PHP_EOL;
		$binding = $this->bindings->getOrCreateBinding($type);
		return new InjectingProvider($this->bindingInstantiator, $binding);
	}

	/**
	 * @param string $type
	 * @return mixed
	 */
	function getInstance($type) {
		return $this->getProvider($type)->get();
	}

	function getParent() {
		// FIXME czy na pewno tak to ma być?
		return $this->parent;
	}

	function createChildInjector($module) {
		throw new RuntimeException("not implemented yet");
	}

	/**
	 * @deprecated old crap
	 * @param $qualifiedClassName
	 * @return object
	 */
	public function createInstance($qualifiedClassName) {
		// TODO remove this shit function which was public
		$class = new ReflectionClass($qualifiedClassName);
		$this->incompleteInstances += array(
			$qualifiedClassName => array()
		);

		if (isset($this->dependencies[$qualifiedClassName])) {
			$incompleteInstance = $class->newInstanceWithoutConstructor();
			$this->incompleteInstances[$qualifiedClassName][] = $incompleteInstance;
			return $incompleteInstance;
		}

		$this->dependencies[$qualifiedClassName] = true;
		$constructorInjector = $this->getConstructorInjector($class);
		$memberInjectors = $this->getMemberInjectors($class);
		$instance = $constructorInjector->createNew();

		foreach ($memberInjectors as $injector) {
			$injector->injectTo($instance);
		}

		foreach ($this->incompleteInstances[$qualifiedClassName] as $incompleteInstance) {
			$constructorInjector->injectTo($incompleteInstance);

			foreach ($memberInjectors as $injector) {
				$injector->injectTo($incompleteInstance);
			}
		}

		unset($this->dependencies[$qualifiedClassName]);
		unset($this->incompleteInstances[$qualifiedClassName]);
		return $instance;
	}

	private function getConstructorInjector(ReflectionClass $class) {
		$dependencies = $this->dependenciesProvider->getConstructorDependencies($class->getName());
		return new ConstructorInjector($class, $dependencies);
	}

	/**
	 *
	 * @param ReflectionClass $class
	 * @return MemberInjector[]
	 */
	private function getMemberInjectors(ReflectionClass $class) {
		return array();
	}
}