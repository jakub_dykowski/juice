<?php
namespace migit\inject\impl;

use migit\inject\binding\BindingVisitor;
use migit\inject\Provider;
use ReflectionClass;

class ProviderInstanceBinding extends AbstractScopedBinding {

	/**
	 * @var Provider
	 */
	private $provider;

	public function __construct(ReflectionClass $source, Provider $provider, ReflectionClass $scope = null) {
		$this->source = $source;
		$this->provider = $provider;
		$this->scope = $scope;
	}

	public function getProvider() {
		return $this->provider;
	}

	public function accept(BindingVisitor $bindingVisitor) {
		return $bindingVisitor->visitProviderInstanceBinding($this);
	}
}
