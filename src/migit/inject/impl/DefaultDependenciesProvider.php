<?php
namespace migit\inject\impl;

use Exception;
use migit\inject\CreationException;
use migit\inject\Injector;
use migit\inject\Provider;
use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;
use RuntimeException;

class DefaultDependenciesProvider implements DependenciesProvider {

	private $injector;

	function __construct(Injector $injector) {
		$this->injector = $injector;
	}

	function getDependencies($qualifiedClassName, $method) {
		$class = new ReflectionClass($qualifiedClassName);
		return $this->getDependenciesOfMethod($class->getMethod($method));
	}

	function getProviderFor($qualifiedClassName) {
		// TODO it looks like its the place where JIT provider should be returned, or does it?
		return new JustInTimeProvider($this->injector, new ReflectionClass($qualifiedClassName));
	}

	function getConstructorDependencies($qualifiedClassName) {
		$class = new ReflectionClass($qualifiedClassName);
		$constructor = $class->getConstructor();
		
		if ($constructor == null) {
			return array ();
		}
		
		return $this->getDependenciesOfMethod($constructor);
	}

	function getDependencyOfField($field) {
		throw new Exception("annotations not supported");
		// TODO annotations:
		// /**
		// *
		// * @var \Sharbat\Inject $injectAnnotation
		// */
		// $injectAnnotation = $field->getFirstAnnotation(Annotations::INJECT);
		
		// if ($injectAnnotation instanceof InjectProvider) {
		// /**
		// *
		// * @var \Sharbat\InjectProvider $injectAnnotation
		// */
		// return $this->getProviderFor($injectAnnotation->getTargetClassName());
		// }
		return $this->injector->getInstance($injectAnnotation->getDependencyName());
	}

	function getDependenciesOfMethod(ReflectionMethod $method) {
		$dependencies = array ();
		
		foreach ($method->getParameters() as $parameter) {
			$dependencies[] = $this->getDependencyOfParameter($parameter);
		}
		
		return $dependencies;
	}

	function getDependencyOfParameter(ReflectionParameter $parameter) {
		
		$class = $parameter->getClass();

		if ($class == null) {
			if ($parameter->isOptional())
				return $parameter->getDefaultValue();
			else
				throw new CreationException('cannot satisfy dependency of argument $' . $parameter->getName()
					. '  which has no class specified inside class ' . $parameter->getClass()->getName()
					. ' (' . $parameter->getClass()->getFileName() . ')');

			// TODO that should probably be removed (old crap I think, since constant?bindings? are not supported)
			return $this->injector->getConstant($parameter->getName());
		}

		// TODO why should be comparing to Provider class?
		if (!$class->getName() !== Provider::class /*'\juice\Provider'*/) {
			return $this->injector->getInstance($class->getName());
		}
		
		// TODO annotations:
		// /**
		// *
		// * @var \Sharbat\Provider[] $providerAnnotations
		// */
		// $providerAnnotations = $parameter->getDeclaringMethod()->getAnnotations(Annotations::PROVIDER);
		
		// foreach ($providerAnnotations as $annotation) {
		// if ($annotation->getParameterName() == $parameter->getName()) {
		// return $this->getProviderFor($annotation->getTargetClassName());
		// }
		// }
		
		throw new RuntimeException('Cannot satisfy Provider dependency. No target type specified');
	}
}
