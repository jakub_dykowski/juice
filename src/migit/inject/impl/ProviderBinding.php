<?php
namespace migit\inject\impl;

use migit\inject\binding\BindingVisitor;
use migit\inject\Injector;
use ReflectionClass;

/**
 * This is OLD, needs to be DELETED.
 *
 * Probably corrected and functioning properly.
 */
class ProviderBinding extends AbstractScopedBinding {

	/**
	 * @var ReflectionClass
	 */
	private $providerClass;

	/**
	 * @var Injector
	 */
	private $injector;

	public function __construct(ReflectionClass $source, ReflectionClass $providerClazz, Injector $injector,
	                            ReflectionClass $scope = null) {
		$this->source = $source;
		$this->providerClass = $providerClazz->getName();
		$this->scope = $scope;
		$this->injector = $injector;
	}

	public function getProvider() {
		// INJECT provider binding uses default getInstance() to get specified provider
		return $this->injector->getInstance($this->providerClass);
	}

	public function accept(BindingVisitor $bindingVisitor) {
		return $bindingVisitor->visitProviderBinding($this);
	}
}
