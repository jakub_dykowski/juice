<?php
namespace migit\inject\impl;

use migit\inject\binding\ScopedBinding;
use ReflectionClass;
use RuntimeException;

abstract class AbstractScopedBinding implements ScopedBinding {

	/**
	 *
	 * @var ReflectionClass
	 */
	protected $source;
	
	/**
	 *
	 * @var ReflectionClass|null
	 */
	protected $scope;

	public function getKey() {
		return $this->source->getName();
	}

	public function getProvider() {
		throw new RuntimeException("probably extending class should implement this method");
	}

	/**
	 *
	 * @return ReflectionClass
	 */
	public function getSource() {
		return $this->source;
	}

	public function setScope(ReflectionClass $scope) {
		$this->scope = $scope;
	}

	public function unsetScope() {
		$this->scope = null;
	}

	/**
	 *
	 * @return ReflectionClass|null
	 */
	public function getScope() {
		return $this->scope;
	}
}
