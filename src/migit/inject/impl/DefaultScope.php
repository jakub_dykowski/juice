<?php
namespace migit\inject\impl;

use migit\inject\Binding;
use migit\inject\binding\BindingVisitor;
use migit\inject\Injector;
use migit\inject\Provider;
// TODO replace with guice one

/**
 * \Sharbat\@Singleton
 */
class DefaultScope implements ShittyScope, BindingVisitor {
	private $injector;

	public function __construct(Injector $injector) {
		$this->injector = $injector;
	}

	public function getInstance(Binding $binding) {
		return $binding->accept($this);
	}

	public function visitLinkedBinding(LinkedBinding $binding) {
		if ($binding->getTarget() != null) {
			return $this->injector->getInstance($binding->getTarget()->getQualifiedName());
		}
		
		return $this->injector->createInstance($binding->getSource()->getQualifiedName());
	}

	public function visitInstanceBinding(InstanceBinding $binding) {
		return $binding->getInstance();
	}

	public function visitProviderBinding(ProviderBinding $binding) {
		throw new \Exception("unfortunately you reached old and unsupported code");
		// TODO there is something seriously wrong here, getProvider() doesn't return a class string, but and object
		$providerClass = $binding->getProvider();
		/**
		 * @var Provider $provider
		 */
		$provider = $this->injector->getInstance($providerClass->getQualifiedName());
		return $provider->get();
	}

	public function visitProviderInstanceBinding(ProviderInstanceBinding $binding) {
		return $binding->getProvider()->get();
	}

	public function visitConstantBinding(ConstantBinding $binding) {
		return $binding->getValue();
	}
}