<?php
namespace migit\inject\impl;

use migit\inject\binding\BindingVisitor;
use migit\inject\Injector;
use ReflectionClass;
use RuntimeException;
use UnexpectedValueException;

/**
 * Binding based on source or target (but how?)
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class LinkedBinding extends AbstractScopedBinding {

	/** @var ReflectionClass */
	private $target;

	/** @var Injector|null */
	private $injector;

	public function __construct(ReflectionClass $source, ReflectionClass $target = null, ReflectionClass $scope = null,
								Injector $injector = null) {
		$this->source = $source;
		$this->target = $target;
		$this->scope = $scope;
		$this->injector = $injector;
	}

	public function getProvider() {
		// TODO remember to fix up
		if ($this->source == null)
			throw new UnexpectedValueException("source should not be null right here");
		if ($this->injector == null)
			throw new RuntimeException("getProvider() not supported for module bindings"); // guice says so

		// figured out that sometimes LinkedBinding don't have target, then we use source as THE class
		return new JustInTimeProvider($this->injector, $this->target ?: $this->source);
	}

	/**
	 *
	 * @return ReflectionClass
	 */
	public function getTarget() {
		return $this->target;
	}

	public function accept(BindingVisitor $bindingVisitor) {
		return $bindingVisitor->visitLinkedBinding($this);
	}
}
