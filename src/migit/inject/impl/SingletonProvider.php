<?php
/**
 * Created by PhpStorm.
 * User: kuba
 * Date: 14.12.15
 * Time: 22:02
 */
namespace migit\inject\impl;

use migit\inject\Provider;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class SingletonProvider implements Provider {

	/**
	 * @var Provider|null
	 */
	private $provider;

	/**
	 * @var object|null
	 */
	private $instance;

	function __construct(Provider $creator) {
		$this->provider = $creator;
	}

	function get() {
		if ($this->instance === null) {
			$provided = $this->provider->get();
			// forget the provider, we don't need him anymore
			unset($this->provider);

			// don't remember proxies; these exist only to serve circular dependencies
			// TODO :inject circular proxy
			// if (isCircularProxy($provided)) {
			// return provided;
			// }

//			$this->instance = $provided === null ? NULL::$INSTANCE : $provided;
			$this->instance = $provided;
		}

//		return $this->instance !== NULL::$INSTANCE ? $this->instance : null;
		return $this->instance;
	}
}