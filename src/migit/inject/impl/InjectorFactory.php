<?php
namespace migit\inject\impl;

use migit\inject\Injector;
use migit\inject\Provider;
use migit\inject\spi\Dependency;
use migit\inject\spi\InternalFactory;

/**
 * FIXME maybe we should make an InstanceFactory for every instances.
 * 
 * @author Jakub Dykowski
 *        
 */
class InjectorFactory implements InternalFactory, Provider {
	private $injector;

	function __construct(Injector $injector = null) {
		$this->injector = $injector;
	}

	function get(/*InternalContext */$context, Dependency $dependency, $linked) {
		return $this->injector;
	}

	function get() {
		return $this->injector;
	}

	function __toString() {
		return "Provider<Injector>";
	}
}