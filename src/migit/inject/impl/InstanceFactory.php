<?php
namespace migit\inject\impl;

use migit\inject\Provider;
use migit\inject\spi\Dependency;
use migit\inject\spi\InternalFactory;

class InstanceFactory implements InternalFactory, Provider {

	private $instance;

	function __construct($instance) {
		$this->instance = $instance;
	}

	function get($context, Dependency $dependency, $linked) {
		return $this->instance;
	}

	function get() {
		return $this->instance;
	}

	function __toString() {
		return "Provider<" . ($this->instance !== null ? get_class($this->instance) : 'null') . ">";
	}
}