<?php
namespace migit\inject\impl;

use migit\inject\Binding;
use migit\inject\binding\ScopedBinding;
use migit\inject\Injector;

/**
 * Probably to be removed (old crap?)
 */
class BindingInstantiator {

	private $defaultScope;
	private $injector;

	public function __construct(DefaultScope $defaultScope, Injector $injector) {
		$this->defaultScope = $defaultScope;
		$this->injector = $injector;
	}

	public function getInstance(Binding $binding) {
		// TODO clear :old implementation
        // should it really be thrown out, maybe its useful
//		 if ($binding instanceof ScopedBinding) {
////			 throw new RuntimeException("scoped bindings not implemented yet");
//		 	return $this->getScopeInstance($binding)->getInstance($binding);
//		 } else {
//		 	return $this->defaultScope->getInstance($binding);
//		 }

		return $binding->getProvider()->get();
	}

	/**
	 *
	 * @param ScopedBinding $binding
	 * @return ShittyScope
	 */
	private function getScopeInstance(ScopedBinding $binding) {
		if ($binding->getScope() != null) {
			$scopeClass = $binding->getScope();
			return $this->injector->getInstance($scopeClass->getName());
		}
		
		return $this->defaultScope;
	}
}
