<?php
namespace migit\inject\impl;

interface MembersInjector {

	function injectTo($instance);
}
