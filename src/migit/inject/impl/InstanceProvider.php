<?php
namespace migit\inject\impl;

use InvalidArgumentException;
use migit\inject\Provider;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class InstanceProvider implements Provider {

	private $instance;

	public function __construct($instance) {
		if (!is_object($instance))
			throw new InvalidArgumentException("not an object");
		$this->instance = $instance;
	}

	public function get() {
		return $this->instance;
	}
}