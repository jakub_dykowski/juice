<?php
namespace migit\inject\impl;

use ReflectionClass;

class ConstructorInjector {

	private $class;
	private $constructorDependencies;

	public function __construct(ReflectionClass $class, array $constructorDependencies) {
		$this->class = $class;
		$this->constructorDependencies = $constructorDependencies;
	}

	public function createNew() {
	    if (DEBUG)
		    echo "instantiating: " . $this->class->getName() . "(" . count($this->constructorDependencies) .
                ")" . PHP_EOL;
		if ($this->class->getConstructor() == null) {
			return $this->class->newInstance();
		} else {
			return $this->class->newInstanceArgs($this->constructorDependencies);
		}
	}

	public function injectTo($instance) {
		$constructor = $this->class->getConstructor();

		if ($constructor != null) {
			$constructor->invokeArgs($instance, $this->constructorDependencies);
		}
	}
}
