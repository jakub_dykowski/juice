<?php
namespace migit\inject\impl;

use migit\inject\Provider;
use migit\inject\reflect\Field;
use migit\inject\reflect\Method;
use migit\inject\reflect\Parameter;
use ReflectionMethod;
use ReflectionParameter;

interface DependenciesProvider {

	/**
	 *
	 * @param string $qualifiedClassName        	
	 * @param string $method        	
	 * @return array
	 */
	function getDependencies($qualifiedClassName, $method);

	/**
	 *
	 * @param string $qualifiedClassName        	
	 * @return Provider
	 */
	function getProviderFor($qualifiedClassName);

	/**
	 *
	 * @param string $qualifiedClassName
	 * @return array
	 */
	function getConstructorDependencies($qualifiedClassName);

	// TODO remove old crap

	/**
	 * old crap!
	 * @deprecated
	 * @param \migit\inject\reflect\Field $field
	 * @return mixed
	 */
	function getDependencyOfField($field);

	/**
	 *
	 * @param \migit\inject\reflect\Method $method
	 * @return array
	 */
	function getDependenciesOfMethod(ReflectionMethod $method);

	/**
	 *
	 * @param \migit\inject\reflect\Parameter $parameter
	 * @return mixed
	 */
	function getDependencyOfParameter(ReflectionParameter $parameter);
}
