<?php
namespace migit\inject\impl;

use migit\inject\ConfigurationException;
use migit\inject\Injector;
use migit\inject\Provider;
use migit\inject\ProvisionException;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionParameter;

/**
 * Provides instance, by trying to satisfy dependencies at runtime.
 * Returns new instance every time.
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class JustInTimeProvider implements Provider {

	/**
	 * @var Injector
	 */
	private $injector;

	/**
	 * @var ReflectionClass
	 */
	private $class;

	public function __construct(Injector $injector, ReflectionClass $class) {
		$this->injector = $injector;
		$this->class = $class;
	}

	function get() {
		try {
			return $this->createNewInstance($this->class);
		} catch (ReflectionException $e) {
			throw new ConfigurationException("couldn't instantiate class: $this->class", 0, $e);
		}
	}

	private function createNewInstance(ReflectionClass $class) {
		$constructor = $class->getConstructor();
		if ($constructor == null) {
			return $class->newInstance();
		} else {
			$dependencies = $this->getMethodDependencies($constructor);
			return $class->newInstanceArgs($dependencies);
		}
	}

	private function getMethodDependencies(ReflectionMethod $method) {
		$dependencies = [];
		foreach ($method->getParameters() as $parameter)
			$dependencies[] = $this->getParameterDependencies($parameter, $method);
		return $dependencies;
	}

	private function getParameterDependencies(ReflectionParameter $parameter, ReflectionMethod $method) {
		$class = $parameter->getClass();
		if ($class == null) {
			if ($parameter->isOptional())
				return $parameter->getDefaultValue();
			else
				throw new ProvisionException('cannot satisfy dependency of argument $' . $parameter->getName()
					. ' which has no class specified in method ' . $method->getName()
					. ' inside class ' . $parameter->getClass()
					. ' (file ' . $method->getFileName() . ')');
			// dead code, probably needs to be deleted
//			return $this->injector->getConstant($parameter->getName());
		}

//		$class = $this->reflectionService->getClass($class->getName());

		// question is when the condition could be true, what would trigger it?
		if ($class->getName() !== Provider::class)
			return $this->injector->getInstance($class->getName());

		// FIXME annotations:
		// /**
		// *
		// * @var \Sharbat\Provider[] $providerAnnotations
		// */
		// $providerAnnotations = $parameter->getDeclaringMethod()->getAnnotations(Annotations::PROVIDER);

		// foreach ($providerAnnotations as $annotation) {
		// if ($annotation->getParameterName() == $parameter->getName()) {
		// return $this->getProviderFor($annotation->getTargetClassName());
		// }
		// }

		throw new ProvisionException('Cannot satisfy Provider dependency. No target type specified');
	}
}