<?php
namespace migit\inject\binder;

interface ScopedBindingBuilder {

	/**
	 * 
	 * @param string $qualifiedClassName of scope
	 */
	function in($qualifiedClassName);

	function inSingleton();

	function inNoScope();
}