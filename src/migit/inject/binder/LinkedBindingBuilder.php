<?php
namespace migit\inject\binder;

use migit\inject\Binding;
use migit\inject\binding\Bindings;
use migit\inject\impl\AbstractScopedBinding;
use migit\inject\impl\InstanceBinding;
use migit\inject\impl\LinkedBinding;
use migit\inject\impl\ProviderBinding;
use migit\inject\impl\ProviderInstanceBinding;
use migit\inject\impl\Singleton;
use migit\inject\Injector;
use migit\inject\Provider;
use ReflectionClass;

class LinkedBindingBuilder implements ScopedBindingBuilder {

	/**
	 * Bindings
	 *
	 * @var Bindings
	 */
	private $bindings;

	/**
	 * Temporary binding that sits here between chained calls to methods of this builder.s
	 * @var AbstractScopedBinding
	 */
	private $binding;

	/**
	 * @FIXME wondering if it should be here
	 * @var Injector
	 */
	private $injector;

	public function __construct(Bindings $bindings, Injector $injector) {
		// FIXME Injector should not be here, only temporary :jacob
		$this->bindings = $bindings;
		$this->injector = $injector;
	}

	public function bind($qualifiedClassName) {
		$class = new ReflectionClass($qualifiedClassName);
		$this->binding = new LinkedBinding($class, null, null,
			$this->injector); // FIXME only configuration (no injector should be here)
		$this->bindings->addBinding($this->binding);

		// :annotations
		// /**
		// * @var \Sharbat\ShittyScope $scopeAnnotation
		// */
		// $scopeAnnotation = $class->getFirstAnnotation(Annotations::SCOPE);
		// if ($scopeAnnotation != null) {
		// $scopeClass = $this->reflectionService->getClass($scopeAnnotation->getScopeClassName());
		// $this->binding->setScope($scopeClass);
		// }

		return $this;
	}

	/**
	 *
	 * @param string $qualifiedClassName
	 * @return self
	 */
	public function to($qualifiedClassName) {
		$targetClass = new ReflectionClass($qualifiedClassName);
		$this->binding = new LinkedBinding($this->binding->getSource(), $targetClass, $this->binding->getScope(),
			$this->injector); // FIXME refactor whole binding impl
		// TODO it should add bindings only to the binder (configuration only, not actual bindings)
		$this->addBinding($this->binding);
		return $this;
	}

	/**
	 *
	 * @param object $instance
	 * @return self
	 */
	public function toInstance($instance) {
		$this->binding = new InstanceBinding($this->binding->getSource(), $instance, $this->binding->getScope());
		$this->addBinding($this->binding);
		return $this;
	}

	/**
	 *
	 * @param string $qualifiedClassName
	 * @return self
	 */
	public function toProvider($qualifiedClassName) {
		$providerClass = new ReflectionClass($qualifiedClassName);
		$this->binding = new ProviderBinding($this->binding->getSource(), $providerClass, $this->injector,
			$this->binding->getScope());
		$this->addBinding($this->binding);
		return $this;
	}

	/**
	 *
	 * @param Provider $provider
	 * @return self
	 */
	public function toProviderInstance(Provider $provider) {
		$this->binding = new ProviderInstanceBinding($this->binding->getSource(), $provider,
			$this->binding->getScope());
		$this->addBinding($this->binding);
		return $this;
	}

	public function in($qualifiedClassName) {
		$class = new ReflectionClass($qualifiedClassName);
		$this->binding->setScope($class);
	}

	public function inSingleton() {
		// weird, earlier there was '\juice\impl\Singleton'
		$this->in(Singleton::class);
	}

	public function inNoScope() {
		$this->binding->unsetScope();
	}

	private function addBinding(Binding $binding) {
		$this->bindings->removeBinding($binding->getKey());
		$this->bindings->addBinding($binding);
	}
}