<?php
namespace migit\inject;

use Exception;

/**
 * Thrown when a programming error such as an illegal binding, or unsupported scope is found.
 * Clients should catch this exception, log it, and stop execution.
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class ConfigurationException extends Exception {

}