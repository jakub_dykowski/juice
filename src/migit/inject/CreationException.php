<?php
namespace migit\inject;

use Exception;

/**
 * Thrown when errors occur while creating an Injector.
 * Probably because injection dependency cannot be satisfied.
 *
 * @author Jakub Dykowski
 */
class CreationException extends Exception {}