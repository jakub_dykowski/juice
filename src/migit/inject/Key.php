<?php
namespace migit\inject;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class Key {
	protected $type;
	
	function __construct($type) {
		$this->type = $type;
	}
	
	function getType() {
		return $this->type;
	}
}