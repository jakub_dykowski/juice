<?php
namespace migit\inject;

use migit\inject\binding\NoScope;
use migit\inject\binding\SingletonScope;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
final class Scopes {
	const SINGLETON = SingletonScope::class;
	const NO_SCOPE = NoScope::class;

	/**
	 * prevents instances
	 */
	private function __construct() {}
}