<?php
namespace migit\inject;

/**
 * The stage we're running in.
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
final class Stage {

	function __construct() {}
	
	/**
	 * We're running in a tool (an IDE plugin for example).
	 * We need binding meta data but not a
	 * functioning Injector. Do not inject members of instances. Do not load eager singletons. Do as
	 * little as possible so our tools run nice and snappy. Injectors created in this stage cannot
	 * be used to satisfy injections.
	 */
	const TOOL = 2;
	
	/**
	 * We want fast startup times at the expense of runtime performance and some up front error
	 * checking.
	 */
	const DEVELOPMENT = 0;
	
	/**
	 * We want to catch errors as early as possible and take performance hits up front.
	 */
	const PRODUCTION = 1;

	/**
	 * @return int[]
	 */
	public static function values() {
		return [
			self::DEVELOPMENT,
			self::PRODUCTION,
			self::TOOL
		];
	}
}

