<?php
namespace migit\inject;

use Exception;
use InvalidArgumentException;
use LogicException;
use migit\inject\binder\LinkedBindingBuilder;
use RuntimeException;

abstract class AbstractModule implements Module {
	
	/**
	 *
	 * @var Binder
	 */
	private $binder;

	/**
	 *
	 * @param Binder $builder
	 * @throws Exception
	 */
	final function configureBinder(Binder $builder) {
		if ($this->binder != null)
			throw new LogicException("Re-entry is not allowed.");
		if ($builder == null)
			throw new InvalidArgumentException('builder cannot be null');
		$this->binder = $builder;
		try {
			$this->configure();
		} catch (Exception $e) {
			$this->binder = null;
			throw $e;
		}
	}

	/**
	 * Configures a {@link Binder} via the exposed methods.
	 */
	protected abstract function configure();

	/**
	 * Gets direct access to the underlying {@code Binder}.
	 *
	 * @return Binder
	 */
	protected function binder() {
		if ($this->binder == null)
		    throw new RuntimeException("The binder can only be used inside configure()");
		return $this->binder;
	}
	
	// /**
	// * @see Binder#bind(Key)
	// */
	// protected <T> LinkedBindingBuilder<T> bind(Key<T> key) {
	// return binder()->bind(key);
	// }
	
	// /**
	// * @see Binder#bind(TypeLiteral)
	// */
	// protected <T> AnnotatedBindingBuilder<T> bind(TypeLiteral<T> typeLiteral) {
	// return binder()->bind(typeLiteral);
	// }
	
	// /**
	// * @see Binder#bind(Class)
	// */
	// protected <T> AnnotatedBindingBuilder<T> bind(Class<T> clazz) {
	// return binder()->bind(clazz);
	// }
	
	// /**
	// * @see Binder#bindConstant()
	// */
	// protected AnnotatedConstantBindingBuilder bindConstant() {
	// return binder()->bindConstant();
	// }

	/**
	 *
	 * @see Binder#install(Module)
	 * @param Module $module
	 */
	protected function install(Module $module) {
		$this->binder()->install($module);
	}

	/**
	 *
	 * @see Binder#addError(String, Object[])
	 */
	protected function addError($message, $argument) {
		// TODO imeplement
		throw new RuntimeException("not implemented yet");
		$this->binder()->addError($message, $arguments);
	}
	
	// /**
	// *
	// * @see Binder#addError(Throwable)
	// */
	// protected function addError(\Exception $t) {
	// $this->binder()->addError($t);
	// }
	
	// /**
	// *
	// * @see Binder#addError(Message)
	// * @since 2.0
	// */
	// protected function addError(Message $message) {
	// $this->binder()->addError($message);
	// }
	
	/**
	 *
	 * @see Binder#requestInjection(Object)
	 * @param $instance object        	
	 * @since 2.0
	 */
	protected function requestInjection($instance) {
		$this->binder()->requestInjection(instance);
	}

	/**
	 *
	 * @see Binder#requestStaticInjection(Class[])
	 * @param string $types,...        	
	 */
	protected function requestStaticInjection($types) {
		$this->binder()->requestStaticInjection($types);
	}
	
	// /*if[AOP]*/
	// /**
	// * @see Binder#bindInterceptor(com.google.inject.matcher.Matcher,
	// * com.google.inject.matcher.Matcher,
	// * org.aopalliance.intercept.MethodInterceptor[])
	// */
	// protected void bindInterceptor(Matcher<? super Class< classMatcher,
	// Matcher<? super Method> methodMatcher,
	// org.aopalliance.intercept.MethodInterceptor... interceptors) {
	// binder()->bindInterceptor(classMatcher, methodMatcher, interceptors);
	// }
	// /*end[AOP]*/
	
	// /**
	// * Adds a dependency from this module to {@code key}.
	// * When the injector is
	// * created, Guice will report an error if {@code key} cannot be injected.
	// * Note that this requirement may be satisfied by implicit binding, such as
	// * a public no-arguments constructor.
	// */
	// protected function requireBinding(Key $key) {
	// $this->binder()->getProvider($key);
	// }
	
	/**
	 * Adds a dependency from this module to {@code type}.
	 * When the injector is
	 * created, Guice will report an error if {@code type} cannot be injected.
	 * Note that this requirement may be satisfied by implicit binding, such as
	 * a public no-arguments constructor.
	 *
	 * @param string $type
	 *        	class
	 */
	protected function requireBinding($type) {
		$this->binder()->getProvider($type);
	}
	
	// /**
	// *
	// * @see Binder#getProvider(Key)
	// * @return Provider
	// */
	// protected function getProvider(Key $key) {
	// return $this->binder()->getProvider($key);
	// }
	
	/**
	 *
	 * @see Binder#getProvider(Class)
	 *
	 */
	protected function getProvider($type) {
		return $this->binder()->getProvider($type);
	}
	/*
	 * /** @see Binder#convertToTypes
	 */
	protected function convertToTypes(Matcher $typeMatcher, TypeConverter $converter) {
		$this->binder()->convertToTypes($typeMatcher, $converter);
	}

	/**
	 *
	 * @see Binder#currentStage()
	 * @return Stage
	 */
	protected function currentStage() {
		return $this->binder()->currentStage();
	}

	/**
	 *
	 * @see Binder#getMembersInjector(Class)
	 * @param string $type
	 *        	class
	 */
	protected function getMembersInjector($type) {
		return $this->binder()->getMembersInjector($type);
	}
	
	/**
	 * @param string $qualifiedClassName
	 * @return LinkedBindingBuilder
	 */
	protected function bind($qualifiedClassName) {
		return $this->binder->bind($qualifiedClassName);
	}
	
	// /**
	// * * @see
	// * Binder#getMembersInjector(TypeLiteral)
	// */
	// protected function getMembersInjector(TypeLiteral $type) {
	// return $this->binder()->getMembersInjector($type);
	// }
}