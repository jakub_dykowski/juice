<?php
namespace migit\inject;

use inject\a;
use inject\locates;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
interface Scope {

	/**
	 * Scopes a provider. The returned provider returns objects from this scope.
	 * If an object does not exist in this scope, the provider can use the given
	 * unscoped provider to retrieve one.
	 *
	 * <p>ShittyScope implementations are strongly encouraged to override
	 * {@link Object#toString} in the returned provider and include the backing
	 * provider's {@code toString()} output.
	 *
	 * @param key binding key
	 * @param unscoped locates an instance when one doesn't already exist in this
	 *  scope.
	 * @return a new provider which only delegates to the given unscoped provider
	 *  when an instance of the requested object doesn't already exist in this
	 *  scope
	 */
	function scope(/*Key $key*/ $type, Provider $unscoped);

	/**
	 * A short but useful description of this scope.  For comparison, the standard
	 * scopes that ship with guice use the descriptions
	 * {@code "Scopes.SINGLETON"}, {@code "ServletScopes.SESSION"} and
	 * {@code "ServletScopes.REQUEST"}.
	 * @return string
	*/
	function __toString();
}
