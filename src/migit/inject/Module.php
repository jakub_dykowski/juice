<?php
namespace migit\inject;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
interface Module {

	/**
	 * Contributes bindings and other configurations for this module to $binder.
	 *
	 * <p><strong>Do not invoke this method directly</strong> to install submodules. Instead use
	 * {@link Binder#install(Module)}, which ensures that {@link Provides provider methods} are
	 * discovered.
	 * @param Binder $binder
	 * @return
	 */
	function configureBinder(Binder $binder);
}