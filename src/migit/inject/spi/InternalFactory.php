<?php
namespace migit\inject\spi;

use inject\spi\com;
use inject\spi\instance;

interface InternalFactory {

	/**
	 * Creates an object to be injected.
	 * 
	 * @param
	 *        	context of this injection
	 * @param bool $linked
	 *        	true if getting as a result of a linked binding
	 *        	
	 * @throws com.google.inject.internal.ErrorsException if a value cannot be provided
	 * @return instance to be injected
	 */
	function get(/*InternalContext TODO :inject*/$context, Dependency $dependency, $linked);
}