<?php
namespace migit\inject\spi;

use inject\spi\Set;

/**
 * Implemented by {@link com.google.inject.Binding bindings}, {@link com.google.inject.Provider
 * providers} and instances that expose their dependencies explicitly.
 *
 * @author jessewilson@google.com (Jesse Wilson)
 * @since 2.0
 */
interface HasDependencies {

	/**
	 * Returns the known dependencies for this type. If this has dependencies whose values are not
	 * known statically, a dependency for the {@link com.google.inject.Injector Injector} will be
	 * included in the returned set.
	 *
	 * @return Set<Dependency> a possibly empty set
	 */
	function getDependencies();
}
