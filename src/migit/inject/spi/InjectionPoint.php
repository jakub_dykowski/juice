<?php
namespace migit\inject\spi;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class InjectionPoint {

	private $optional;
	private $member;
	private $declaringType;
 	private $dependencies;
 	
 	function __construct($declaringType,\ReflectionFunction $memberReflection, $optional) {
 		$this->declaringType = $declaringType;
 		$this->member = $memberReflection;
 		$this->optional = $optional;$memberReflection->
 		
 		// TODO init dependencies :inject
 		
 	}
 	
 	/**
 	 * @return array if Dependency (set)
 	 */
 	private function forMember(\ReflectionFunction $member, $type) {
 	
 	    $dependencies = array();
 	    $index = 0;
 		
 	   // for (TypeLiteral<?> parameterType : type.getParameterTypes(member) {
 	    for (TypeLiteral<?> parameterType : type.getParameterTypes(member) {
 	      try {
 	        Annotation[] parameterAnnotations = annotationsIterator.next();
 	        Key<?> key = Annotations.getKey(parameterType, member, parameterAnnotations, errors);
 	        dependencies.add(newDependency(key, Nullability.allowsNull(parameterAnnotations), index));
 	        index++;
 	      } catch (ConfigurationException $e) {
 	        errors.merge(e.getErrorMessages());
 	      } catch (ErrorsException e) {
 	        errors.merge(e.getErrors());
 	      }
 	    }
 	
 	    errors.throwConfigurationExceptionIfErrorsExist();
 	    return ImmutableList.copyOf(dependencies);
 	  }
 	  
   /**
   * Returns the dependencies for this injection point. If the injection point is for a method or
   * constructor, the dependencies will correspond to that member's parameters. Field injection
   * points always have a single dependency for the field itself.
   *
   * @return a possibly-empty list
   */
  public List<Dependency<?>> getDependencies() {
    return dependencies;
  }

  /**
   * Returns true if this injection point shall be skipped if the injector cannot resolve bindings
   * for all required dependencies. Both explicit bindings (as specified in a module), and implicit
   * bindings ({@literal @}{@link com.google.inject.ImplementedBy ImplementedBy}, default
   * constructors etc.) may be used to satisfy optional injection points.
   */
  public boolean isOptional() {
    return optional;
  }
}