<?php
namespace migit\inject\spi;

/**
 * A variable that can be resolved by an injector.
 *
 * <p>Use {@link #get} to build a freestanding dependency, or {@link InjectionPoint} to build one
 * that's attached to a constructor, method or field.
 *
 * @author crazybob@google.com (Bob Lee)
 * @author jessewilson@google.com (Jesse Wilson)
 * @since 2.0
 */
final class Dependency {
	/**
	 *
	 * @var InjectionPoint
	 */
	private $injectionPoint;
	/**
	 *
	 * @var string
	 */
	private $type;
	/**
	 *
	 * @var bool
	 */
	private $nullable;
	/**
	 *
	 * @var int
	 */
	private $parameterIndex;

	function __construct(InjectionPoint $injectionPoint = null, $type, $nullable, $parameterIndex) {
		$this->injectionPoint = $injectionPoint;
		$this->key = $key;
		$this->nullable = $nullable;
		$this->parameterIndex = $parameterIndex;
	}

	/**
	 * Returns a new dependency that is not attached to an injection point.
	 * The returned dependency is
	 * nullable.
	 *
	 * @return Dependency of the $type type
	 */
	static function get($type) {
		return new Dependency(null, $type, true, -1);
	}

	/**
	 * Returns the dependencies from the given injection points.
	 *
	 * @param array $injectrionPoints
	 *        	of InjectionPoint
	 * @return array of Dependency
	 */
	static function forInjectionPoints(array $injectionPoints) {
		$dependencies = array ();
		foreach ($injectionPoints as $injectionPoint)
			foreach ($injectionPoint . getDependencies() as $dependency)
				array_push($dependencies, $dependency);
		return $dependencies;
	}

	/**
	 * Returns the key to the binding that satisfies this dependency.
	 *
	 * @return string type
	 */
	function getKey() {
		return $this->type;
	}

	/**
	 * Returns true if null is a legal value for this dependency.
	 */
	function isNullable() {
		return $this->nullable;
	}

	/**
	 * Returns the injection point to which this dependency belongs, or null if this dependency isn't
	 * attached to a particular injection point.
	 *
	 * @return InjectionPoint
	 */
	function getInjectionPoint() {
		return $this->injectionPoint;
	}

	/**
	 * Returns the index of this dependency in the injection point's parameter list, or {@code -1} if
	 * this dependency does not belong to a parameter list.
	 * Only method and constuctor dependencies
	 * are elements in a parameter list.
	 *
	 * @return int
	 */
	function getParameterIndex() {
		return $this->parameterIndex;
	}
}