<?php
namespace migit\inject\spi;

use migit\inject\Binding;

/**
 * A binding to a single instance.
 * The same instance is returned for every injection.
 *
 * @since 2.0
 */
interface InstanceBinding extends Binding, HasDependencies {

	/**
	 * Returns the user-supplied instance.
	 */
	function getInstance();

	/**
	 * Returns the field and method injection points of the instance, injected at injector-creation
	 * time only.
	 *
	 * @return InjectionPoint Set, a possibly empty set
	 */
	function getInjectionPoints();
}
