<?php
namespace migit\inject;

/**
 * 
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 * @license MIT
 */
interface Injector {

	/**
	 * Injects dependencies into the fields and methods of {@code instance}.
	 * Ignores the presence or
	 * absence of an injectable constructor.
	 *
	 * <p>Whenever Guice creates an instance, it performs this injection automatically (after first
	 * performing constructor injection), so if you're able to let Guice create all your objects for
	 * you, you'll never need to use this method.
	 *
	 * @param
	 *        	instance to inject members on
	 *        	
	 * @see Binder#getMembersInjector(Class) for a preferred alternative that supports checks before
	 *      run time
	 */
	// FIXME może jednak to warto, ale przecież nie interesuje mnie wstrzykiwanie pól, jeszcze
//	function injectMembers($instance);
	
	// /**
	// * Returns the members injector used to inject dependencies into methods and fields on instances
	// * of the given type {@code T}.
	// *
	// * @param TypeLiteral typeLiteral type to get members injector for
	// * @see Binder#getMembersInjector(TypeLiteral) for an alternative that offers up front error
	// * detection
	// * @return MembersInjector
	// * @since 2.0
	// */
	// function getMembersInjector(TypeLiteral $typeLiteral);
	
	// /**
	// * Returns the members injector used to inject dependencies into methods and fields on instances
	// * of the given type {@code T}. When feasible, use {@link Binder#getMembersInjector(TypeLiteral)}
	// * instead to get increased up front error detection.
	// *
	// * @param string $type to get members injector for
	// * @see Binder#getMembersInjector(Class) for an alternative that offers up front error
	// * detection
	// * @return MembersInjector
	// * @since 2.0
	// */
	// function getMembersInjector($type);
	
	// /**
	// * Returns this injector's <strong>explicit</strong> bindings.
	// *
	// * <p>The returned map does not include bindings inherited from a {@link #getParent() parent
	// * injector}, should one exist. The returned map is guaranteed to iterate (for example, with
	// * its {@link Map#entrySet()} iterator) in the order of insertion. In other words, the order in
	// * which bindings appear in user Modules.
	// *
	// * <p>This method is part of the Guice SPI and is intended for use by tools and extensions.
	// *
	// * @return array map<Key, Binding>
	// */
	// function getBindings();
	
	// /**
	// * Returns a snapshot of this injector's bindings, <strong>both explicit and
	// * just-in-time</strong>.
	// * The returned map is immutable; it contains only the bindings that were
	// * present when {@code getAllBindings()} was invoked. Subsequent calls may return a map with
	// * additional just-in-time bindings.
	// *
	// * <p>The returned map does not include bindings inherited from a {@link #getParent() parent
	// * injector}, should one exist.
	// *
	// * <p>This method is part of the Guice SPI and is intended for use by tools and extensions.
	// *
	// * @return array map<Key, Binding>
	// * @since 3.0
	// */
	// function getAllBindings();
	
	// /**
	// * Returns the binding for the given injection key.
	// * This will be an explicit bindings if the key
	// * was bound explicitly by a module, or an implicit binding otherwise. The implicit binding will
	// * be created if necessary.
	// *
	// * <p>This method is part of the Guice SPI and is intended for use by tools and extensions.
	// *
	// * @return Binding binding
	// * @throws ConfigurationException if this injector cannot find or create the binding.
	// */
	// function getBinding(Key $key);
	
	// /**
	// * Returns the binding for the given type.
	// * This will be an explicit bindings if the injection key
	// * was bound explicitly by a module, or an implicit binding otherwise. The implicit binding will
	// * be created if necessary.
	// *
	// * <p>This method is part of the Guice SPI and is intended for use by tools and extensions.
	// *
	// * @param string $type
	// * @return Binding
	// * @throws ConfigurationException if this injector cannot find or create the binding.
	// * @since 2.0
	// */
	// function getBinding($type);
	
	// /**
	// * Returns the binding if it already exists, or null if does not exist.
	// * Unlike
	// * {@link #getBinding(Key)}, this does not attempt to create just-in-time bindings
	// * for keys that aren't bound.
	// *
	// * <p> This method is part of the Guice SPI and is intended for use by tools and extensions.
	// *
	// * @return Binding
	// * @since 3.0
	// */
	// function getExistingBinding(Key $key);
	
	// /**
	// * Returns all explicit bindings for {@code type}.
	// *
	// * <p>This method is part of the Guice SPI and is intended for use by tools and extensions.
	// *
	// * @return Binding list
	// */
	// function findBindingsByType(TypeLiteral $type);
	
	// /**
	// * Returns the provider used to obtain instances for the given injection key.
	// * When feasible, avoid
	// * using this method, in favor of having Guice inject your dependencies ahead of time.
	// *
	// * @return Provider
	// * @throws ConfigurationException if this injector cannot find or create the provider.
	// * @see Binder#getProvider(Key) for an alternative that offers up front error detection
	// */
	// function getProvider(Key $key);
	
	/**
	 * Returns the provider used to obtain instances for the given type.
	 * When feasible, avoid
	 * using this method, in favor of having Guice inject your dependencies ahead of time.
	 *
	 * @param string $type        	
	 * @return Provider
	 * @throws ConfigurationException if this injector cannot find or create the provider.
	 * @see Binder#getProvider(Class) for an alternative that offers up front error detection
	 */
	function getProvider($type);
	
	// /**
	// * Returns the appropriate instance for the given injection key; equivalent to {@code
	// * getProvider(key).get()}.
	// * When feasible, avoid using this method, in favor of having Guice
	// * inject your dependencies ahead of time.
	// *
	// * @return unknown instance
	// * @throws ConfigurationException if this injector cannot find or create the provider.
	// * @throws ProvisionException if there was a runtime failure while providing an instance.
	// */
	// function getInstance(Key $key);

	/**
	 * Returns the appropriate instance for the given injection type; equivalent to {@code
	 * getProvider(type).get()}.
	 * When feasible, avoid using this method, in favor of having Guice
	 * inject your dependencies ahead of time.
	 *
	 * @param string $type
	 *        	class name
	 * @throws \migit\inject\ConfigurationException if this injector cannot find or create the provider.
	 * @throws ProvisionException if there was a runtime failure while providing an instance.
	 */
	function getInstance($type);

	/**
	 * Returns this injector's parent, or null if this is a top-level injector.
	 *
	 * @return Injector
	 * @since 2.0
	 */
	function getParent();

	/**
	 * Returns a new injector that inherits all state from this injector.
	 * All bindings, scopes,
	 * interceptors and type converters are inherited -- they are visible to the child injector.
	 * Elements of the child injector are not visible to its parent.
	 *
	 * <p>Just-in-time bindings created for child injectors will be created in an ancestor injector
	 * whenever possible. This allows for scoped instances to be shared between injectors. Use
	 * explicit bindings to prevent bindings from being shared with the parent injector. Optional
	 * injections in just-in-time bindings (created in the parent injector) may be silently
	 * ignored if the optional dependencies are from the child injector.
	 *
	 * <p>No key may be bound by both an injector and one of its ancestors. This includes just-in-time
	 * bindings. The lone exception is the key for {@code Injector.class}, which is bound by each
	 * injector to itself.
	 *
	 * @param mixed $module,...
	 *        	[optional] unlimited number of Module or (one) array of them
	 * @return Injector
	 * @since 2.0
	 */
	function createChildInjector($module);
	
	// /**
	// * Returns a map containing all scopes in the injector. The maps keys are scoping annotations
	// * like {@code Singleton.class}, and the values are scope instances, such as {@code
	// * Scopes.SINGLETON}. The returned map is immutable.
	// *
	// * <p>This method is part of the Guice SPI and is intended for use by tools and extensions.
	// *
	// * @since 3.0
	// */
	// Map<Class<? extends Annotation>, ShittyScope> getScopeBindings();
	
	// /**
	// * Returns a set containing all type converter bindings in the injector. The returned set is
	// * immutable.
	// *
	// * <p>This method is part of the Guice SPI and is intended for use by tools and extensions.
	// *
	// * @since 3.0
	// */
	// Set<TypeConverterBinding> getTypeConverterBindings();
}