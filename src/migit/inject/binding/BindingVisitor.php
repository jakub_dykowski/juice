<?php
namespace migit\inject\binding;

use migit\inject\impl\ConstantBinding;
use migit\inject\impl\InstanceBinding;
use migit\inject\impl\LinkedBinding;
use migit\inject\impl\ProviderBinding;
use migit\inject\impl\ProviderInstanceBinding;

interface BindingVisitor {

	function visitLinkedBinding(LinkedBinding $binding);

	function visitInstanceBinding(InstanceBinding $binding);

	function visitProviderBinding(ProviderBinding $binding);

	function visitProviderInstanceBinding(ProviderInstanceBinding $binding);

	/**
	 * @deprecated constant binding are not supported
	 * @param ConstantBinding $binding
	 * @return mixed
	 */
	function visitConstantBinding(ConstantBinding $binding);
}