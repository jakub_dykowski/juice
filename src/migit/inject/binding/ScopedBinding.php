<?php
namespace migit\inject\binding;

use migit\inject\Binding;
use ReflectionClass;

/**
 * @deprecated
 */
interface ScopedBinding extends Binding {

	/**
	 * @return ReflectionClass
	 */
	function getScope();

	function setScope(ReflectionClass $scope);

	function unsetScope();
}