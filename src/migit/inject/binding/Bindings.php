<?php
namespace migit\inject\binding;

use migit\inject\Binding;
use migit\inject\impl\ConstantBinding;

interface Bindings {

	function addBinding(Binding $binding);

	/**
	 * @param string $key
	 * @return Binding|null
	*/
	function getBinding($key);

	/**
	 * @param string $constant
	 * @return ConstantBinding
	*/
	function getConstantBinding($constant);

	/**
	 * @param string $key
	 * @return Binding
	*/
	function getOrCreateBinding($key);

	/**
	 * @return Bindings[]
	*/
	function getBindings();

	function removeBinding($key);
}