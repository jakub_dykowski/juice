<?php
namespace migit\inject\binding;

use inject\binding\Provider;
use migit\inject\Scope;

class NoScope implements Scope {

	function scope($type, Provider $unscoped) {
		return $unscoped;
	}

	/**
	 * A short but useful description of this scope.
	 * For comparison, the standard
	 * scopes that ship with guice use the descriptions
	 * {@code "Scopes.SINGLETON"}, {@code "ServletScopes.SESSION"} and
	 * {@code "ServletScopes.REQUEST"}.
	 *
	 * @return string
	 */
	function __toString() {
		return 'Scopes.NO_SCOPE';
	}
}