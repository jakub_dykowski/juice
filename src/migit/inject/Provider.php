<?php
namespace migit\inject;

interface Provider {
	
	/**
	 * Returns object(s) provided by this provider.
	 *
	 * @return object
	 */
	public function get();
}